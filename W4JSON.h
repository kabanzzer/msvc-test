#pragma once

#include <cstdlib>
#include <type_traits>
#include "FatalError.h"

template<typename T>
const char* JsonExceptionWrapper(T e)
{
    return e.what();
}

template<typename T>
const char* JsonExceptionWrapper(T* e)
{
    return e->what();
}

#define JSON_THROW_USER(exception) FATAL_ERROR("nlohmann exception: %s", JsonExceptionWrapper(exception)); std::abort();
#define JSON_TRY_USER if(true)
#define JSON_CATCH_USER(exception) if(false)
#define JSON_INTERNAL_CATCH_USER(exception) if(false)

#include "external/nlohmann/json.hpp"
