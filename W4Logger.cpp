#include "W4Common.h"
#include "W4Logger.h"

#include <cstdarg>

const char* w4::utils::unsafe_fmt(const char *fmt, ...)
{
    constexpr const int buffsize =  1024;
    static char buffer[buffsize] = {0};
    static const char * badstr = "bad string";
    va_list args;
    va_start(args, fmt);
    auto res = vsnprintf(buffer, buffsize, fmt, args);
    va_end(args);
    if(res < 0 || res > buffsize)
        return badstr;
    return buffer;
}

#ifdef __EMSCRIPTEN__
    W4_JS_IMPORT
    {
        void w4lLog(int const logLevel, char const* const file, int const line, char const* const message);
        void w4lLogInit(const char* deviceId, const char* remoteAddress);
    }

    void w4::logger::log(const uint8_t logLevel, char const* const file, int const line, char const* const message)
    {
        w4lLog(logLevel, file, line, message);
    }
    void w4::logger::initialize(const std::string& deviceId, const std::string& remoteAddress)
    {
        w4lLogInit(deviceId.data(), remoteAddress.empty() ? nullptr : remoteAddress.data());
    }
#else

#include <iostream>
#include <string>



#ifdef _WIN32
    static const char* logLevelNames[] =
    {
        "[T] ",
        "[D] ",
        "[I] ",
        "[W] ",
        "[E] "
    };
    static const char* endline = "\n";
#else
    static const char* logLevelNames[] =
    {
        "\\33[0;34m[T] ",
        "\\33[0;36m[D] ",
        "\\33[0;32m[I] ",
        "\\33[0;33m[W] ",
        "\\33[0;31m[E] "
    };
    static const char* endline = "\\33[0m\n";
#endif

void w4::logger::log(const uint8_t logLevel, char const* const file, int const line, char const* const message)
{
    std::cout << logLevelNames[logLevel]  << message;
    if(logLevel >= W4_LOG_LEVEL_WARNING)
    {
        std::cout << " (" << file << ":" << line << ")";
    }
    std::cout << endline;
}

void w4::logger::initialize(const std::string& deviceId, const std::string& remoteAddress)
{
    //do nothing
}

#endif


