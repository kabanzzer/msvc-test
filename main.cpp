
#include "W4Logger.h"
#include "W4JSON.h"

using uint = unsigned int;

enum class myEnum {One, Two, Three, Four};


struct pod
{
    pod() = default;
    pod(uint a, uint b, myEnum type): a(a), b(b), type(type) {}
    void log(const std::string& prefix) { W4_LOG_TRACE("%s - %d:%d::%d", prefix.c_str(),a, b, static_cast<int>(type)); }

    myEnum type;

    uint a, b;
};

NLOHMANN_JSON_SERIALIZE_ENUM(myEnum, {
{ myEnum::One,   "one" },
{ myEnum::Two,   "two" },
{ myEnum::Three, "three" },
{ myEnum::Four,  "four" },
    });

namespace nlohmann {
  


template<>
struct adl_serializer<pod>
{
    static void to_json(json& j, const pod &v)
    {
        j = json
        {
            {"a", v.a},
            {"b", v.b},
            {"type", v.type}
        };
    }
    static void from_json(const json& j, pod& v)
    {
        j.at("a").get_to(v.a);
        j.at("b").get_to(v.b);
        j.at("type").get_to(v.type);
    }
};

}

using namespace nlohmann;

namespace w4
{
    template<typename T>
    void storeValue(nlohmann::json& dst, const T& value) {
        nlohmann::adl_serializer<T>::to_json(dst, value);
    }

    template<typename T>
    void loadValue(const nlohmann::json& src, T& value) {
        nlohmann::adl_serializer<T>::from_json(src, value);
    }

    
}


int main()
{

    pod a(1,2, myEnum::One), b(3,4, myEnum::Two), c(5,6, myEnum::Three);

    a.log("A");
    b.log("B");
    c.log("C");



    json j;

        w4::storeValue(j, a);
        w4::loadValue(j, b);

    a.log("A`");
    b.log("B`");
    c.log("C`");


         
 return 0;
}



