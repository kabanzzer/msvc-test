#include "W4Common.h"
#include "FatalError.h"

#ifndef __EMSCRIPTEN__
    #include <stdexcept>
#endif

void w4::internal_assert(const char *why, const char *file, int line, const char * func)
{
    w4::abort(w4::utils::format("W4 ASSERT: %s [%d] : %s ( %s)", file, line, func, why));
}

#ifdef __EMSCRIPTEN__

    W4_JS_IMPORT {
        void w4lAbort(char const*const message);

    }


    void w4::abort(const std::string& message) { w4lAbort(message.c_str());}

#else
    void w4::abort(const std::string& message)
    {
        W4_LOG_ERROR(message.c_str());
        exit(-1);
    }
#endif

